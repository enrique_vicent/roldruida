var app = angular.module('druida', [
    'ui.router',
    'LocalStorageModule'
]);

app.config([
    '$stateProvider',
    '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('home', {
                url: '/home',
                templateUrl: 'template/home.html'
            })
            .state('hechizos',{
                url: '/hechizos',
                templateUrl: '/template/hechizos.html',
                controller: 'hechizoCtrl',
                resolve: {
                    hechizosPromise: ['hechizos', function(hechizos){
                        return hechizos.getAll();
                    }]
                }
            })
            .state('magias',{
                url: '/magias',
                templateUrl: '/template/magias.html',
                controller: 'magiasCtrl',
                resolve: {
                    hechizosPromise: ['magias', function(magias){
                        return magias.getAll();
                    }]
                }
            })
            .state('animales',{
                url: '/animales',
                templateUrl: '/template/animales.html',
                controller: 'animalesCtrl',
                resolve:{
                    aliadosPromise: ['aliados', function(aliados){
                        return aliados.getAll();
                    }]
                }
            })
            .state('inventario',{
                url: '/inventario',
                templateUrl: 'template/inventario.html',
                controller: 'inventarioCtrl',
                resolve: {
                    inventarioPromise: ['inventario', function(inventario){
                        return inventario.getAll();
                    }]
                }
            });
        $urlRouterProvider.otherwise('home');
    }
]);

app.config([
    'localStorageServiceProvider',
    function(localStorageServiceProvider){
        localStorageServiceProvider.setPrefix('rol')
    }
]);