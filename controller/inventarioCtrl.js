app.controller('inventarioCtrl', [
    '$scope',
    'inventario',
    function($scope,inventario){
        $scope.bolsa = inventario.bolsa;
        $scope.$watch('bolsa', function(){
            inventario.saveBolsa();
        },true);
        $scope.bolsaAdd = function (){
            nuevo=inventario.createItem($scope.nuevoNombre,$scope.nuevaDescripcion,$scope.nuevasCargas);
            $scope.bolsa.push(nuevo);
        };
        $scope.bolsaRemove = function(indice){
            $scope.bolsa.splice(indice,1);
        };
        $scope.bolsaDecrementa = function(item){
            item.cargas -- ;
        };
    }
]);