app.controller('hechizoCtrl', [
    '$scope',
    'hechizos',
    function($scope,hechizos){
        $scope.hechizos = hechizos.hechizos;
        $scope.preparacion = hechizos.preparacion;
        $scope.otrosDiarios = hechizos.otros;
        $scope.preseleccionados = hechizos.preseleccionados;
        
        $scope.addSpell = function (hechizo){
            el = angular.copy(hechizo);
            el.activo=true;
            $scope.preparacion[hechizo.nivel].preparados.push(el);
        };
        $scope.removeSpell = function (lista,indice){
            lista.splice(indice,1);
        };
        $scope.getAvaliableSpellsQuantity = function (level){
            levelList = $scope.preparacion[level];
            return levelList.capacidad - levelList.preparados.length;
        }; 
        $scope.$watch('preparacion', function(){
            hechizos.savePreparation();
        },true);
        $scope.addOther = function(){
            if($scope.nuevoOtro){
                nuevo={nombre:$scope.nuevoOtro, activo:true};
                $scope.otrosDiarios.push(nuevo);
                $scope.nuevoOtro="";
            }
        };
        $scope.removeOther = function(indice){
            $scope.otrosDiarios.splice(indice, 1);
        };
        $scope.$watch('otrosDiarios',function(){
            hechizos.saveOtherItems();
        },true);
        $scope.preselect = function (hechizo) {
            $scope.preseleccionados.push(hechizo);
            $scope.preseleccionados.sort(function(a,b){
                return a.nivel-b.nivel;
            });
            hechizos.savePreselected();
        };
        $scope.depreselect = function (hechizo) {
            var indice = $scope.preseleccionados.lastIndexOf(hechizo);
            $scope.preseleccionados.splice(indice,1);
            hechizos.savePreselected();
        };
        $scope.use = function(hechizo){
            hechizo.activo=false;
        };
        $scope.resetDia = function(){
            desmarca = function(element, index, array){
                element.activo = true;
            };
            $scope.otrosDiarios.forEach(desmarca);
            $scope.preparacion.forEach(function(nivel,numNivel){
                nivel.preparados.forEach(desmarca);
            });
            if($scope.snd){}
            else{
                $scope.snd = new Audio("/resources/pray.mp3"); // buffers automatically when created
            }
            $scope.snd.play();
        };
        $scope.addable=function(spell){
            var avaliable = $scope.getAvaliableSpellsQuantity(spell.nivel);
            if (avaliable >0){
                return 'yes';
            }
            else {
                var pre;
                for(pre in $scope.preseleccionados){
                    if ($scope.preseleccionados[pre].nombre == spell.nombre){
                        return 'no';
                    }
                }
                return 'star'
            }
        };
        $scope.cantidadDePergaminos = function (hechizo){
            return hechizos.getNumberScrollsOfSpell(hechizo);
        };
        $scope.crearPergamino = function (hechizo) {
            hechizos.addScrollOfSpell (hechizo);  
            hechizos.scrollsSave();
        };
        $scope.usarPergamino = function (hechizo) {
            hechizos.removeScrollOfSpell (hechizo);
            hechizos.scrollsSave();
        }
        $scope.listaDePergaminos = function () {
            return Object.keys( hechizos.pergaminos ).map(function (key) {return hechizos.pergaminos[key]});
        }
    }
]);