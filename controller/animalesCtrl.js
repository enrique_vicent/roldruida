app.controller('animalesCtrl', [
    '$scope',
    'aliados',
    function ($scope, aliados) {
        $scope.posibles = aliados.posibles;
        $scope.invocados = aliados.invocados;
        $scope.deltas = [-20, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, +1, +2, 3 ,4 ,5 ,6 ,7 ,8 ,9 ,10 ,20];
        $scope.nivel = 5;
        $scope.hitPoint = function calculaHitpoint(bicho) {
            var result = 1;
            var defaultresult = bicho.hp;
            var formula = bicho.dg;
            while (formula.indexOf('d') !== -1) {
                formula = formula.replace("d", "*rollDice"); 
            }  
            function rollDice(num) {
                return Math.floor(Math.random() * num) + 1;
            }
            result = eval(formula);
            if (result > 0) {
                return result;
            } else {
                return defaultresult;
            }
        };
        $scope.invocarManual = function() {
            var bichoInvocado = {};
            bichoInvocado.animal = $scope.nnombre;
            bichoInvocado.aliado = 0;
            bichoInvocado.resisteConjuros = 0;
            bichoInvocado.tamano = $scope.ntamano;
            bichoInvocado.referencia = $scope.nreferencia;
            bichoInvocado.hp = $scope.nhp0;
            bichoInvocado.dg = $scope.nhp0;
            bichoInvocado.ca = 0
            bichoInvocado.ataque = $scope.nataque;
            bichoInvocado.ataqueCompleto = $scope.nataqueCompleto;
            bichoInvocado.salvacion = {}
            bichoInvocado.salvacion.fortaleza = $scope.nfortaleza;
            bichoInvocado.salvacion.reflejos = $scope.nreflejos;
            bichoInvocado.salvacion.voluntad = $scope.nvoluntad;
            bichoInvocado.especial = $scope.nespecial
        
            bichoInvocado.hp0 = $scope.nhp0;
            bichoInvocado.turnos = $scope.nivel;
            bichoInvocado.perpetuo = $scope.nperpetuo;
            $scope.invocados.push(bichoInvocado);            
            
        };
        $scope.invocar = function(bicho) {
            var bichoInvocado = angular.copy(bicho);
            bichoInvocado.hp0 = $scope.hitPoint(bicho);
            bichoInvocado.hp = bichoInvocado.hp0;
            bichoInvocado.turnos = $scope.nivel;
            bichoInvocado.perpetuo = false;
            $scope.invocados.push(bichoInvocado);
        };
        $scope.liberar =function(bicho) {
            $scope.invocados.splice($scope.invocados.indexOf(bicho),1);
        };
        $scope.deltaHp = function(dhp,bicho){
            bicho.hp = bicho.hp + dhp;
        };
        $scope.pasaTurno =function(){
            var i ;
            for ( i=$scope.invocados.length-1; i>=0 ; i --){
                bichoInvocado = $scope.invocados[i];
                if(!bichoInvocado.perpetuo && 0<=bichoInvocado.turnos){
                    bichoInvocado.turnos --;
                }
                if(0>bichoInvocado.turnos)
                {
                    $scope.liberar(bichoInvocado);
                }
            }
        };
        $scope.$watch('invocados',function(){
            aliados.saveInvocados();
        },true);
    }
]);