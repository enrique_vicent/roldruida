app.controller('magiasCtrl', [
    '$scope',
    'magias',
    function($scope,magias){
        $scope.hechizos = magias.hechizos;
        $scope.preparacion = magias.preparacion;
        $scope.otrosDiarios = magias.otros;
        $scope.preseleccionados = magias.preseleccionados;
        
        $scope.addSpell = function (hechizo){
            el = angular.copy(hechizo);
            el.activo=true;
            $scope.preparacion[hechizo.nivel].preparados.push(el);
        };
        $scope.removeSpell = function (lista,indice){
            lista.splice(indice,1);
        };
        $scope.getAvaliableSpellsQuantity = function (level){
            levelList = $scope.preparacion[level];
            return levelList.capacidad - levelList.preparados.length;
        }; 
        $scope.$watch('preparacion', function(){
            magias.savePreparation();
        },true);
        $scope.addOther = function(){
            if($scope.nuevoOtro){
                nuevo={nombre:$scope.nuevoOtro, activo:true};
                $scope.otrosDiarios.push(nuevo);
                $scope.nuevoOtro="";
            }
        };
        $scope.removeOther = function(indice){
            $scope.otrosDiarios.splice(indice, 1);
        };
        $scope.$watch('otrosDiarios',function(){
            magias.saveOtherItems();
        },true);
        $scope.preselect = function (hechizo) {
            $scope.preseleccionados.push(hechizo);
            $scope.preseleccionados.sort(function(a,b){
                return a.nivel-b.nivel;
            });
            magias.savePreselected();
        };
        $scope.depreselect = function (hechizo) {
            var indice = $scope.preseleccionados.lastIndexOf(hechizo);
            $scope.preseleccionados.splice(indice,1);
            magias.savePreselected();
        };
        $scope.use = function(hechizo){
            hechizo.activo=false;
        };
        $scope.resetDia = function(){
            desmarca = function(element, index, array){
                element.activo = true;
            };
            $scope.otrosDiarios.forEach(desmarca);
            $scope.preparacion.forEach(function(nivel,numNivel){
                nivel.preparados.forEach(desmarca);
            });
            if($scope.snd){}
            else{
                $scope.snd = new Audio("/resources/pray.mp3"); // buffers automatically when created
            }
            $scope.snd.play();
        };
        $scope.addable=function(spell){
            var avaliable = $scope.getAvaliableSpellsQuantity(spell.nivel);
            if (!$scope.estaEnLibro(spell)){
                return 'no';
            }
            else if (avaliable >0){
                return 'yes';
            }
            else {
                var pre;
                for(pre in $scope.preseleccionados){
                    if ($scope.preseleccionados[pre].nombre == spell.nombre){
                        return 'no';
                    }
                }
                return 'star'
            }
        };
        $scope.estaEnLibro = function (magia){
            return magias.isInBook(magia);
        };
        $scope.escribirHechizo = function (hechizo){
            magias.bookAppend(hechizo);
            magias.removeScrollOfSpell(hechizo);
            magias.scrollsSave();
            magias.bookSave();
        };
        $scope.borrarHechizo = function (hechizo){
            if(confirm("Estas seguro de que quieres borrar este hechizo?")){
                magias.bookRemove(hechizo);
                magias.bookSave();
            }
        };
        $scope.cantidadDePergaminos = function (hechizo){
            return magias.getNumberScrollsOfSpell(hechizo);
        };
        $scope.crearPergamino = function (hechizo) {
            magias.addScrollOfSpell (hechizo);  
            magias.scrollsSave();
        };
        $scope.usarPergamino = function (hechizo) {
            magias.removeScrollOfSpell (hechizo);
            magias.scrollsSave();
        }
        $scope.listaDePergaminos = function () {
            return Object.keys( magias.pergaminos ).map(function (key) {return magias.pergaminos[key]});
        }
    }
]);