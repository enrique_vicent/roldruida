app.factory('hechizos', [
    '$http', 
    'localStorageService',
    function($http,localStorageService){
        var o = {
            hechizos: [],
            preparacion: [],
            preseleccionados: [],
            otros: [],
            pergaminos: {}
        };
        o.getAll = function() {
            o.getSpells();
            o.getPreparation();
            o.getOtherItems();
            o.getPreselected();
            o.scrollsLoad();
        };
        o.getSpells = function() {
            return $http.get('/resources/magiaDruida.json').success(function(data){
                angular.copy(data, o.hechizos);
            });
        };
        o.getPreparation = function() {
            storedPreparation = localStorageService.get('hechizos.preparacion');
            o.preparacion =  storedPreparation || [
                {capacidad:2,preparados:[]},
                {capacidad:1,preparados:[]},
                {capacidad:0,preparados:[]},
                {capacidad:0,preparados:[]},
                {capacidad:0,preparados:[]},
                {capacidad:0,preparados:[]},
                {capacidad:0,preparados:[]},
                {capacidad:0,preparados:[]},
                {capacidad:0,preparados:[]},
                {capacidad:0,preparados:[]}
            ];
        };
        o.savePreparation = function() {
            localStorageService.set('hechizos.preparacion',o.preparacion);
        };
        
        //----------
        // gemas
        o.getOtherItems = function() {
            var storedOthers = localStorageService.get('hechizos.otros');
            o.otros =  storedOthers || [];
        };
        o.saveOtherItems = function() {
            localStorageService.set('hechizos.otros',o.otros);
        };
        
        //----------
        // preselected
        o.savePreselected = function() {
            localStorageService.set('hechizos.preselected',o.preseleccionados);
        };
        o.getPreselected = function() {
            var storedPreselected = localStorageService.get('hechizos.preselected');
            o.preseleccionados = storedPreselected || [];
        };
        
        //----------
        // scrolls
        o.addScrollOfSpell = function (spell){
            var spellID = o._getSpellUID(spell);
            if (spellID in o.pergaminos){
                o.pergaminos[spellID].cantidad ++ ;
            } else {
                var nuevo = {cantidad: 1, magia: spell}
                o.pergaminos[spellID] = nuevo;
            }
        };
        o.removeScrollOfSpell = function (spell){
            var id = o._getSpellUID(spell);
            o.pergaminos[id].cantidad -- ;
            if ( o.pergaminos[id].cantidad < 1 ){
                delete o.pergaminos[id];
            }
        };
        o.getNumberScrollsOfSpell = function (spell){
            var spellID = o._getSpellUID(spell);
            var result = 0;
            if (spellID in o.pergaminos){
                result = o.pergaminos[spellID].cantidad;
            };
            return result;
        };
        o.scrollsSave = function (){
            localStorageService.set('hechizos.scrolls',o.pergaminos);
        };
        o.scrollsLoad = function (){
            var lScrolls = localStorageService.get('hechizos.scrolls');
            o.pergaminos = lScrolls || {};
        };
        o._getSpellUID = function (spell){
            return spell.nivel.toString().concat(spell.nombre);
        }
        return o;
}]);