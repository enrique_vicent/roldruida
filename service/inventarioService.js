app.factory('inventario', [ 
    'localStorageService',
    function(localStorageService){
        var o = {
            bolsa: []
        };
        o.getAll = function() {
            o.getBolsa();
        };
        
        o.getBolsa = function() {
            stored = localStorageService.get('inventario.bolsa');
            o.bolsa =  stored || [];
        };
        o.saveBolsa = function() {
            localStorageService.set('inventario.bolsa',o.bolsa);
        };
        o.createItem = function (nombre, descripcion, cargas){
            return {nombre:nombre,descripcion:descripcion,cargas:cargas};
        };
        return o;
}]);