app.factory('aliados', [
    '$http',
    'localStorageService',
    function($http,localStorageService){
        var o = {
            posibles: [],
            invocados: []
        };
        o.getAll = function(){
            o.getPosibles();
            o.getInvocados();
        };
        o.getPosibles = function(){
            return $http.get('/resources/aliados.json').success(function(data){
                angular.copy(data, o.posibles);
            });
        };
        o.saveInvocados = function(){
            localStorageService.set('aliados.invocados',o.invocados);
        };
        o.getInvocados = function(){
            var stored = localStorageService.get('aliados.invocados');
            o.invocados =  stored || [];
        };
        return o;
}]);