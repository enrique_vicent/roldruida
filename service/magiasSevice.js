app.factory('magias', [
    '$http', 
    'localStorageService',
    function($http,localStorageService){
        var o = {
            hechizos: [],
            preparacion: [],
            preseleccionados: [],
            otros: [],
            libro: [],
            pergaminos: {}
        };
        o.getAll = function() {
            o.getSpells();
            o.getPreparation();
            o.getOtherItems();
            o.getPreselected();
            o.bookLoad();
            o.scrollsLoad();
        };
        o.getSpells = function() {
            return $http.get('/resources/magiaMago.json').success(function(data){
                angular.copy(data, o.hechizos);
            });
        };
        o.getPreparation = function() {
            storedPreparation = localStorageService.get('magias.preparacion');
            o.preparacion =  storedPreparation || [
                {capacidad:2,preparados:[]},
                {capacidad:1,preparados:[]},
                {capacidad:0,preparados:[]},
                {capacidad:0,preparados:[]},
                {capacidad:0,preparados:[]},
                {capacidad:0,preparados:[]},
                {capacidad:0,preparados:[]},
                {capacidad:0,preparados:[]},
                {capacidad:0,preparados:[]},
                {capacidad:0,preparados:[]}
            ];
        };
        o.savePreparation = function() {
            localStorageService.set('magias.preparacion',o.preparacion);
        };
        o.getOtherItems = function() {
            var storedOthers = localStorageService.get('magias.otros');
            o.otros =  storedOthers || [];
        };
        o.saveOtherItems = function() {
            localStorageService.set('magias.otros',o.otros);
        };
        o.savePreselected = function() {
            localStorageService.set('magias.preselected',o.preseleccionados);
        };
        o.getPreselected = function() {
            var storedPreselected = localStorageService.get('magias.preselected');
            o.preseleccionados = storedPreselected || [];
        };
        o.bookAppend = function (spell){
            if (o.isInBook(spell)) return;
            var spellId = o._getSpellUID(spell);
            o.libro.push(spellId);
        };
        o._getSpellUID = function (spell){
            return spell.nivel.toString().concat(spell.nombre);
        }
        o._findSpellInBook = function (spell){
            return o.libro.indexOf(o._getSpellUID(spell));
        }
        o.bookRemove = function (spell){
            var index = o._findSpellInBook(spell);
            if (index !== -1){
                o.libro.splice(index,1);
            }
        };
        o.isInBook = function (spell){
            if (spell.libro) return true;
            return o._findSpellInBook(spell) !== -1 ;
        };
        o.bookSave = function (){
            localStorageService.set('magias.libro',o.libro);
        };
        o.bookLoad = function (){
            var libro = localStorageService.get('magias.libro');
            o.libro = libro || [];
        };
        o.addScrollOfSpell = function (spell){
            var spellID = o._getSpellUID(spell);
            if (spellID in o.pergaminos){
                o.pergaminos[spellID].cantidad ++ ;
            } else {
                var nuevo = {cantidad: 1, magia: spell}
                o.pergaminos[spellID] = nuevo;
            }
        };
        o.removeScrollOfSpell = function (spell){
            var id = o._getSpellUID(spell);
            o.pergaminos[id].cantidad -- ;
            if ( o.pergaminos[id].cantidad < 1 ){
                delete o.pergaminos[id];
            }
        };
        o.getNumberScrollsOfSpell = function (spell){
            var spellID = o._getSpellUID(spell);
            var result = 0;
            if (spellID in o.pergaminos){
                result = o.pergaminos[spellID].cantidad;
            };
            return result;
        };
        o.scrollsSave = function (){
            localStorageService.set('magias.scrolls',o.pergaminos);
        };
        o.scrollsLoad = function (){
            var lScrolls = localStorageService.get('magias.scrolls');
            o.pergaminos = lScrolls || {};
        };
        return o;
}]);